#include "counting.h"
#include <cstdio>
#include <cassert>
#include <thrust/scan.h>
#include <thrust/find.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#include <device_launch_parameters.h>

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

template<typename T>
struct MyStruct
{
	__host__ __device__ 
		bool operator()(const T &lhs, const T &rhs) const{	return rhs != '\n';}

};

void CountPosition1(const char *text, int *pos, int text_size)
{
	thrust::device_ptr<char> txt_ptr((char*)text);
	thrust::device_ptr<int> txt_pos((int*)pos);
	thrust::fill(txt_pos, txt_pos+text_size, 1);
	thrust::device_ptr<char> Seg = thrust::find(txt_ptr, txt_ptr + text_size, '\n');
	MyStruct<char>binary_pred;
	thrust::exclusive_scan_by_key(Seg, txt_ptr + text_size, txt_pos + (Seg - txt_ptr), txt_pos + (Seg - txt_ptr), 0, binary_pred);
	thrust::inclusive_scan_by_key(txt_ptr, Seg, txt_pos, txt_pos, binary_pred);

}

__global__ void Counting(const char *text, int *pos, int text_size)
{
	const int y = blockIdx.y * blockDim.y + threadIdx.y;
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	int a = y*7200+x;
	int id = a;
	int count = 0;
	if (a < text_size) {
		while (text[id] != '\n' && id >= 0) {
			id--;
			count++;
		}
	}
	pos[a] = count;

}


void CountPosition2(const char *text, int *pos, int text_size)
{
	dim3 block(240,240);
	dim3 thread(30,30);
	Counting << <block, thread >> >(text, pos, text_size);

}

