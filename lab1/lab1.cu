#include "lab1.h"
#include <cmath>
static const unsigned W = 1920;
static const unsigned H = 1080;
static const unsigned NFRAME = 240;
using namespace std;

__device__ float T = 0.0f;
__device__ bool downmode=false;

#define DIM 1000
#define PI 3.1415926

struct cuComplex {
    float   r;
    float   i;
    __device__ cuComplex( float a, float b ) : r(a), i(b)  {}
    __device__ float magnitude2( void ) {
        return r * r + i * i;
    }
    __device__ cuComplex operator*(const cuComplex& a) {
        return cuComplex(r*a.r - i*a.i, i*a.r + r*a.i);
    }
    __device__ cuComplex operator+(const cuComplex& a) {
        return cuComplex(r+a.r, i+a.i);
    }
};

__device__ int julia( int x, int y ) {
    const float scale = 1.5;
    float jx = scale * (float)(DIM/2 - x)/(DIM/2);
    float jy = scale * (float)(DIM/2 - y)/(DIM/2);

    cuComplex c(-0.4+cos(T), 0.6-sin(T));
    cuComplex a(jx, jy);

    int i = 0;
    for (i=0; i<200; i++) {
        a = a * a * a + c;
        if (a.magnitude2() > 1000)
            return 0;
    }

    return 1;
}

__device__ void RGBtoYUV(uint8_t* yuv, int x, int y, int r, int g, int b){

    //Y
    yuv[y*W+x] = 0.299 * r + 0.587 * g + 0.114 * b;

    if (y % 2 == 0 && x % 2 == 0) {
		int offset = y * W / 4 + x / 2;
		// u
		yuv[W * H + offset] = -0.169 * r - 0.331 * g + 0.500 * b + 128;
		// v
		yuv[W * H * 5 / 4 + offset] = 0.500 * r - 0.419 * g - 0.081 * b + 128;
	}
}

__global__ void kernel(uint8_t* yuv) {
    // map from blockIdx to pixel position
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

    // now calculate the value at that position
    int juliaValue = julia( x, y );
    RGBtoYUV(yuv, x, y, 100*juliaValue+T*150, 150*juliaValue+T*100, 250);

	
	if (downmode==false) {
		T += 0.000001f;
		if(T>=PI/2)
		downmode = true;
	}
	if (downmode == true)
	{
		T -= 0.000001f;
		if (T <= 0)
			downmode = false;
	}
}

struct Lab1VideoGenerator::Impl {
	
};

Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {
}

Lab1VideoGenerator::~Lab1VideoGenerator() {}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};


void Lab1VideoGenerator::Generate(uint8_t *yuv) {
    dim3 grid (W, H) ;
    kernel <<<grid ,1 >>>(yuv) ;
}
